$(document).ready(function() {
    // console.log('in document ready');

    // Handle hearts processing on articles
    $('.js-like-article').on('click', function(e) {
        // console.log('in click handler');
        e.preventDefault();

        var $link = $(e.currentTarget);
        $link.toggleClass('fa-heart-o fa-heart');

        $.ajax({
            method: 'POST',
            url: $link.attr('href')
        }).done(function(data) {
            // console.log(data);
            $('.js-like-article-count').html(data.hearts);
        })
    });
});
