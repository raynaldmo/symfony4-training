<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ArticleStatsCommand extends Command
{
    protected static $defaultName = 'article:stats';

    protected function configure()
    {
        $this
            ->setDescription('Returns some article stats')
            ->addArgument('slug', InputArgument::REQUIRED, 'The article\'s slug')
            ->addOption('format', null, InputOption::VALUE_REQUIRED, 'The output format', 'text')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $slug = $input->getArgument('slug');

        // Add some dummy data for testing the command
        $data = [
            'slug' => $slug,
            'hearts' => random_int(10,100),
        ];
        switch ($input->getOption('format')) {
            case 'text':
                // $io->listing($data);
                $rows = array();
                foreach ($data as $key => $val) {
                    // $rows[] = [$key, $val];
                    array_push($rows, array($key, $val));
                }
                $io->table(['Key', 'Value'], $rows);
                break;
            case 'json':
                $io->write(\GuzzleHttp\json_encode($data), true);
                break;
            default:
                throw new \Exception('Unknown format');
        }

        return 0;
    }
}
