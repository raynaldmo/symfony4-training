<?php


namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use DavidBadura\FakerMarkdownGenerator\FakerProvider;
abstract class BaseFixture extends Fixture
{
    /** @var  ObjectManager */
    private $manager;

    /** @var  \Faker\Generator */
    protected $faker;

    /**
     * load() is called when bin/console doctrine:fixtures:load is executed
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->faker->addProvider(new FakerProvider($this->faker));

        $this->loadData($manager);
    }

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     *
     * @return mixed
     */
    abstract protected function loadData(ObjectManager $manager);

    /**
     * Method to create multiple instances of an object.
     * We call createMany() and pass it the class we want to create,
     * how many we want to create, and a callback method that will be called
     * each time an object is created.
     * That'll be our chance to load that object with data.
     *
     * @param string $className
     * @param int $count
     * @param callable $factory
     */
    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 1; $i <= $count; $i++) {
            $entity = new $className();
            $factory($entity, $i);
            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className.'_'.$i, $entity);
        }
    }

}