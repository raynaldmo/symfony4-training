<?php

namespace App\DataFixtures;

use App\Entity\Article;
use function Clue\StreamFilter\fun;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use App\Entity\Tag;

class ArticleFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $titles = [
        'Why Asteroids Taste Like Bacon',
        'Life on Planet Mercury: Tan, Relaxing and Fabulous',
        'Light Speed Travel: Fountain of Youth or Fallacy',
    ];

    private static $author = [
        'Bugs Bunny',
        'Porky Pig',
        'Tweety Bird',
        'Sylvester',
        'Daffy Duck'
    ];

    private static $images = ['asteroid.jpeg', 'mercury.jpeg' , 'lightspeed.png'];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Article::class,
            20,
            function (Article $article, $count) use ($manager) {
                $article->setTitle($this->faker->randomElement(self::$titles))
                        // ->setSlug($this->faker->slug)
                        // ->setContent($this->faker->paragraph(20, false))
                        ->setContent($this->faker->markdown())
                        ->setAuthor($this->faker->randomElement(self::$author))
                        ->setHeartCount($this->faker->numberBetween(5,100))
                        ->setImageFilename($this->faker->randomElement(self::$images));

//                $comment1 = new Comment();
//                $comment1->setAuthorName('Bugs Bunny');
//                $comment1->setContent('I like bacon too! Buy some from my site! bakinsomebacon.com');
//                $comment1->setArticle($article);
//
//                // This also works
//                $article->addComment($comment1);
//
//                $manager->persist($comment1);
//
//                $comment2 = new Comment();
//                $comment2->setAuthorName('Porky Pig');
//                $comment2->setContent('Woohoo! I\'m going on an all-asteroid diet!');
//                $comment2->setArticle($article);
//
//                // This also works
//                $article->addComment($comment2);
//
//                $manager->persist($comment2);

                // Add some tags to article
                $count = $this->faker->numberBetween(0,5);
                for ($i=0; $i<$count; $i++) {
                    $tag = new Tag();
                    $tag->setName($this->faker->realText(16));
                    $article->addTag($tag);
                    $manager->persist($tag);
                }

                // publish most articles
                if ($this->faker->boolean(70)) {
                    $article->setPublishedAt(
                        $this->faker->dateTimeBetween('-100 days', '-1 days')
                    );
                }
            }
        );

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            TagFixture::class
        ];
    }

}
