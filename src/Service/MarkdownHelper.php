<?php


namespace App\Service;

use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class MarkdownHelper
{
    /**
     * @var \Michelf\MarkdownInterface
     */
    private  $markdown;

    /**
     * @var \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    private  $cache;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $markdownLogger;

    /**
     * @var bool
     */
    private $isDebug;

    /**
     * MarkdownHelper constructor.
     *
     * @param \Michelf\MarkdownInterface $markdown
     * @param \Symfony\Component\Cache\Adapter\AdapterInterface $cache
     * @param \Psr\Log\LoggerInterface $markdownLogger
     */
    public function __construct(MarkdownInterface $markdown,
        AdapterInterface $cache, LoggerInterface $markdownLogger, bool $isDebug)
    {
        $this->markdown = $markdown;
        $this->cache = $cache;
        $this->logger = $markdownLogger;
        $this->isDebug = $isDebug;
    }

    /**
     * @param string $source
     *
     * @return string
     */
    public function parse(string $source) : string  {

        if(stripos($source, 'bacon') !== false) {
            $this->logger->info('They are talking about bacon!');
        }

        // skip caching entirely in debug
        if ($this->isDebug) {
            return $this->markdown->transform($source);
        }

        $item = $this->cache->getItem('markdown_' . md5($source));
        if (!$item->isHit()) {
            $item->set(($this->markdown->transform($source)));
            $this->cache->save($item);
        }
        return $item->get();
    }

}