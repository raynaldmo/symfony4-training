<?php


namespace App\Service;


use App\Helper\LoggerTrait;
use Nexy\Slack\Client;

class SlackClient
{
    use LoggerTrait;

    /**
     * @var \Nexy\Slack\Client
     */
    private $slack;

    /**
     * SlackClient constructor.
     *
     * @param \Nexy\Slack\Client $slack
     */
    public function __construct( Client $slack)
    {
        $this->slack = $slack;
    }


    /**
     * @param string $from
     * @param string $message
     */
    public function sendMessage(string $from, string $message)
    {
        $this->logInfo('Sending a message to Slack!', [
            'message' => $message
        ]);

        $message = $this->slack->createMessage()
                               ->from($from)
                               ->withIcon(':ghost:')
                               ->setText($message);
        $this->slack->sendMessage($message);
    }
}