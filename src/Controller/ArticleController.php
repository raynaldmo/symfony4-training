<?php


namespace App\Controller;


use App\Repository\ArticleRepository;
use App\Service\MarkdownHelper;
use App\Service\SlackClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;

class ArticleController extends AbstractController
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger )
    {
        $this->logger = $logger;
        $this->logger->info('ArticleController instantiated');
    }

    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage(ArticleRepository $repository, LoggerInterface $logger)
    {
        $logger->info('Inside ArticleController');

        /** @var Article $articles */
        // $articles = $repository->findBy(array(), ['publishedAt' => 'DESC']);
        $articles = $repository->findAllPublishedOrderedByNewest();

        // return new Response('<h1 style="color:darkred">This is my first page!</h1>');
        return $this->render('article/homepage.html.twig', [
            'articles' => $articles
        ]);
    }


//    /**
//     * @Route("/news/{slug}", name="article_show")j
//     *
//     * Note: Using Article entity class as type-hint will direct Symfony to automatically
//     * query for one article based on slug
//     *
//     * This is done via the ParamConversion annotation
//     */
//    public function show(Article $article, SlackClient $slack)
//    {
//        if ($article->getSlug() ==='slack') {
//            // $slack->sendMessage('Rocko', rand(1,50) . '/ You the man Raynald!');
//        }
//
//        // dump($article); die;
//
//        // Unused
//        // $debug = $this->getParameter('kernel.debug');
//
//        return $this->render('article/show.html.twig', [
//            'article' => $article,
//         ]);
//    }


   /**
     * @Route("/news/{slug}", name="article_show")j
     */
    public function show($slug, SlackClient $slack, ArticleRepository $repository)
    {
        if ($slug ==='slack') {
            // $slack->sendMessage('Rocko', rand(1,50) . '/ You the man Raynald!');
        }

        /** @var Article $article */
        $article = $repository->findOneBy(['slug' => $slug]);

        if (!$article) {
            throw $this->createNotFoundException(sprintf(
                'No article for slug "%s"', $slug
            ));
        }

        // dump($article); die;

        $comments = array(
            'I ate a normal rock once. It did NOT taste like bacon!',
            'Woohoo! I\'m going on an all-asteroid diet!',
            'I like bacon too! Buy some from my site! bakinsomebacon.com',
        );

        // Unused
        // $debug = $this->getParameter('kernel.debug');

        return $this->render('article/show.html.twig', [
            'article' => $article,
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     */
    public function toggleArticleHeart(Article $article, LoggerInterface $logger, EntityManagerInterface $em)
    {
        // TODO - actually heart/unheart the article!

        $logger->info('Article is being "hearted"');
        $article->incrementHeartCount();

        // Look ma! No need to persist as this is done automatically by Doctrine since
        // we passed in Article object / entity into the controller method
        // $em->persist($article);
        $em->flush();

        $response = $this->json(['hearts' => $article->getHeartCount()]);
        if ( ! $response instanceof Response) {
            // dump($response);
            die();
        }

        return $response;
    }

}