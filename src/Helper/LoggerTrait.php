<?php


namespace App\Helper;


use Psr\Log\LoggerInterface;

trait LoggerTrait
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    // Note that the required annotation is needed to force
    // setLogger to be called when the Trait is instantiated
    // Cool!
    /**
     * @required
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    /**
     * @param string $message
     * @param array $context
     */
    private function logInfo(string $message, array $context = array()) {
        if ($this->logger) {
            $this->logger->info($message, $context);
        }

    }
}