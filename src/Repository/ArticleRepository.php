<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[]
     */
    public function findAllPublishedOrderedByNewest()
    {
        $qb = $this->createQueryBuilder('a');

        $result =  $this->addIsPublishedQueryBuilder($qb)
            ->leftJoin('a.tags', 't')
            ->addSelect('t')
            ->orderBy('a.publishedAt', 'DESC')
            // ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        return $result;
    }


    /**
     * @param \Doctrine\ORM\QueryBuilder $qb
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function addIsPublishedQueryBuilder(QueryBuilder $qb) {
        return $qb->andWhere('a.publishedAt IS NOT NULL');
    }


    public static function createNonDeletedCriteria() : Criteria {
        // Use Doctrine Criteria feature
        return $criteria = Criteria::create()
                   ->andWhere(Criteria::expr()->eq('isDeleted', false))
                    ->orderBy(['createdAt' => 'DESC'])
        ;
    }

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
