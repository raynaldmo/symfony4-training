<?php


namespace App\Twig;


use App\Service\MarkdownHelper;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    /**
     * @var \App\Service\MarkdownHelper
     */
    private $helper;

    public function __construct( MarkdownHelper $helper)
    {
        $this->helper = $helper;
    }

    public function processMarkdown($value)
    {
        return $this->helper->parse($value);
    }

}